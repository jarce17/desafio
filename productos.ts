// productos.ts
// Creando una lista de productos
interface Producto {
    id?: number;
    nombre: string;
    precio: number;
    stock: number;
}
//
const Producto1: Producto = {
    id: 0,
    nombre: "Yogurt",
    precio: 100,
    stock: 13
};
const Producto2: Producto = {
    id: 1,
    nombre: "Paquete de Galletas",
    precio: 89.9,
    stock: 40
};
const Producto3: Producto = {
    id: 2,
    nombre: "Lata Coca-Cola",
    precio: 80.50,
    stock: 0
};
const Producto4: Producto = {
    id: 3,
    nombre: "Alfajor",
    stock: 12,
    precio: 40
}


let productos: Array<Producto> = [
    Producto1,
    Producto2,
    Producto3,
    Producto4
]
/* console.log(productos);

function getStock(productos: Array<Producto>) {

    for (let i = 0; i < productos.length; i++) {
        if (productos[i].stock === 0) {
            console.log(`Lo sentimos, no hay mas unidades del Producto ${productos[i].nombre}`);
        } else {
            console.log(`Del Producto ${productos[i].nombre} quedan ${productos[i].stock} unidades`);
        }
    }
} */
//getStock(productos)

export function getStock(){
    return productos;
}

export function storeProductos(body: any, id: any){
    productos.push({
        id: id,
        nombre: body.nombre,
        stock: body.stock,
        precio: body.precio
    })
}

export function deleteProductos(indice: number){
    productos.splice(indice)
}

export function updateProductos(indice: number, body: any){
    if(body.nombre) productos[indice].nombre = body.nombre;
    if(body.precio) productos[indice].precio = body.precio;
    if(body.stock) productos[indice].stock = body.stock;
}
