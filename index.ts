import express from 'express';
import * as productos from './productos';

//*Iniciamos una app de express
const app = express();
app.use(express.json());

//*Definimos una ruta y su handler correspondiente
app.get('/hello', function (request, response) {
    response.send('Hello World');
});

//*Escuchamos la app de express
app.listen(3000, function () {
    console.log('Servidor escuchando en el puerto 3000!');
});


//*Creamos los endpoints
app.get('/productos', function (request, response) {
    response.send(productos.getStock())
})

app.post('/productos', function (request, response) {
    const body = request.body;
    const id = productos.getStock().length;
    productos.storeProductos(body, id);
    response.send('Agregamos un producto a la lista')
})

app.delete('/productos/:id', function (request, response) {
    const idProducto= Number(request.params.id);
    if (idProducto >= 0 && idProducto < productos.getStock().length) {
    productos.deleteProductos(idProducto);
    response.send('Borramos un producto de la lista')
    } else {
        response.send('El producto no existe')
    }
})

app.put('/productos/:id', function (request, response) {
    const idProducto = Number(request.params.id);
    const body = request.body;
    if (idProducto >= 0 && idProducto < productos.getStock().length) {
        productos.updateProductos(idProducto, body);
        response.send('Actualizamos un producto de la lista')
    } else {
        response.send('El producto no existe')
    }
})